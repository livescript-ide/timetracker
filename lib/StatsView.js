var EventEmitter, StatsView;
EventEmitter = require('events');
StatsView = (function(superclass){
  var prototype = extend$((import$(StatsView, superclass).displayName = 'StatsView', StatsView), superclass).prototype, constructor = StatsView;
  module.exports = StatsView;
  function StatsView(){
    this.element = document.createElement('div');
  }
  StatsView.prototype.destroy = function(){
    this.element.remove();
  };
  StatsView.prototype.getURI = function(){
    return 'timetracker://stats';
  };
  StatsView.prototype.getDefaultLocation = function(){
    return 'right';
  };
  StatsView.prototype.getTitle = function(){
    return 'Timetracker (stats)';
  };
  StatsView.prototype.off = function(){};
  StatsView.prototype.closeButton = function(){
    return "<div class='block timetracker-stats-close-button'><button class='btn'>Close</button></div>";
  };
  StatsView.prototype.uncommited = function(files){
    files == null && (files = []);
    return files.map(function(it){
      return "<p><span class='status-" + it.type + " icon icon-diff-" + it.type + "'></span><span class='timetracker-time'>" + it.time + "</span><span>" + it.percent + " </span><span>" + it.filename + "</span></p>";
    }).join('');
  };
  StatsView.prototype.status = function(status){
    if (!status) {
      return "";
    } else {
      return "<h2>" + status.name + "</h2><p>total: <span class='timetracker-time'>" + status.total + "</span></p><h2>Uncommited changes</h2><div class='timetracker-files'>" + this.uncommited(status.files) + "</div>";
    }
  };
  StatsView.prototype.render = function(state){
    var this$ = this;
    this.element.innerHTML = "<div class='timetracker-stats'><h1><span class='icon icon-clock'></span></h1>" + this.status(state != null ? state.status : void 8) + "" + this.closeButton() + "</div>";
    this.element.querySelector('.timetracker-stats-close-button').addEventListener('click', function(){
      this$.emit('close_clicked');
    });
  };
  return StatsView;
}(EventEmitter));
function extend$(sub, sup){
  function fun(){} fun.prototype = (sub.superclass = sup).prototype;
  (sub.prototype = new fun).constructor = sub;
  if (typeof sup.extended == 'function') sup.extended(sub);
  return sub;
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
//# sourceMappingURL=/home/bartek/Projekty/atom/livescript-ide/timetracker/lib/StatsView.js.map
