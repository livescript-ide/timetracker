var exec, execInRepository, getActiveRepository, getCurrentRepository, execInRepo, mapGitStatus, fileStatus, parseGitChangeType, git, parseChange, parseChanges, parseCommits, slice$ = [].slice, join$ = [].join;
execInRepository = function(repository, command, name){
  var options;
  exec == null && (exec = require('child_process').exec);
  options = {
    cwd: repository.getWorkingDirectory()
  };
  return new Promise(function(resolve, reject){
    exec(command, options, function(error, stdout){
      if (error) {
        return reject(error);
      } else {
        return resolve(stdout);
      }
    });
  });
};
getActiveRepository = function(){
  var editor, activePath, directories, directory, this$ = this;
  if (editor = atom.workspace.getActiveTextEditor()) {
    activePath = editor.getPath();
    directories = atom.project.getDirectories().filter(function(it){
      return it.contains(activePath);
    });
    if (directory = directories[0]) {
      return atom.project.repositoryForDirectory(directory);
    }
  }
};
getCurrentRepository = async function(){
  var repository;
  repository = (await getActiveRepository());
  if (!repository) {
    repository = atom.project.getRepositories()[0];
  }
  return repository;
};
execInRepo = async function(command, name){
  var repository;
  if (repository = (await getCurrentRepository())) {
    return execInRepository(repository, command, name);
  }
};
({
  parseType: function(it){
    switch (it) {
    case 'r':
      return 'removed';
    case 'modified':
      return 'removed';
    }
  }
});
mapGitStatus = function(it){
  switch (it) {
  case 'untracked':
    return 'added';
  default:
    return it != null ? it : 'added';
  }
};
fileStatus = function(arg$, gitStatus){
  var i$, time, percent, type, filename, this$ = this;
  time = 0 < (i$ = arg$.length - 3) ? slice$.call(arg$, 0, i$) : (i$ = 0, []), percent = arg$[i$], type = arg$[i$ + 1], filename = arg$[i$ + 2];
  return {
    filename: filename,
    percent: percent,
    time: join$.call(time, ' '),
    type: mapGitStatus(
    gitStatus.filter(function(it){
      return it.filename === filename;
    }).map(function(it){
      return it.type;
    })[0])
  };
};
parseGitChangeType = function(it){
  switch (it) {
  case 'M':
    return 'modified';
  case 'A':
    return 'added';
  case 'AM':
    return 'added';
  case 'R':
    return 'removed';
  case '??':
    return 'untracked';
  }
};
git = {
  status: async function(){
    var status, statusLines, this$ = this;
    status = (await execInRepo('git status -s'));
    statusLines = status.split('\n');
    return statusLines.map(function(it){
      return it.trim();
    }).map(function(it){
      return it.split(/\s+/);
    }).map(function(arg$){
      var type, filename;
      type = arg$[0], filename = arg$[1];
      return {
        filename: filename,
        type: parseGitChangeType(type)
      };
    });
  }
};
parseChange = function(arg$){
  var i$, time, percent, type, filename;
  time = 0 < (i$ = arg$.length - 3) ? slice$.call(arg$, 0, i$) : (i$ = 0, []), percent = arg$[i$], type = arg$[i$ + 1], filename = arg$[i$ + 2];
  return {
    filename: filename,
    percent: percent,
    time: join$.call(time, ' '),
    type: type
  };
};
parseChanges = function(changes){
  return changes.map(function(it){
    return parseChange(
    it.trim().split(/\s+/));
  });
};
parseCommits = function(lines){
  var insideCommit, emptyLines, commits, current, i$, len$, line;
  insideCommit = false;
  emptyLines = 0;
  commits = [];
  current = [];
  for (i$ = 0, len$ = lines.length; i$ < len$; ++i$) {
    line = lines[i$];
    if (line === "") {
      emptyLines++;
      if (emptyLines === 2) {
        commits.push(current);
        current = [];
        emptyLines = 0;
      }
    } else {
      current.push(line);
    }
  }
  return commits.map(function(arg$){
    var shortHashAndMessage, dateAndAuthor, i$, changes, summary, x$, result;
    shortHashAndMessage = arg$[0], dateAndAuthor = arg$[1], changes = 2 < (i$ = arg$.length - 1) ? slice$.call(arg$, 2, i$) : (i$ = 2, []), summary = arg$[i$];
    x$ = result = {
      changes: parseChanges(changes)
    };
    x$.shortHash = shortHashAndMessage.substring(0, 7);
    x$.message = shortHashAndMessage.substring(8);
    return result;
  });
};
module.exports = {
  fullReport: async function(){
    var report, ref$, reportCommits, commits;
    report = (await execInRepo('git log --pretty=%H --since="100000 days ago" | gtm report'));
    ref$ = report.split(/\n/), reportCommits = slice$.call(ref$, 1);
    return commits = parseCommits(reportCommits);
  },
  status: async function(){
    var ref$, status, gitStatus, statusLines, i$, files, lastLine, totalTime, repositoryName;
    ref$ = (await Promise.all([execInRepo('gtm status'), git.status()])), status = ref$[0], gitStatus = ref$[1];
    statusLines = status.split('\n').filter(function(it){
      return it !== '[0;0m' && it !== '[0m' && it !== "";
    });
    files = 0 < (i$ = statusLines.length - 1) ? slice$.call(statusLines, 0, i$) : (i$ = 0, []), lastLine = statusLines[i$];
    ref$ = lastLine.trim().split(/\s+/), totalTime = 0 < (i$ = ref$.length - 1) ? slice$.call(ref$, 0, i$) : (i$ = 0, []), repositoryName = ref$[i$];
    return {
      name: repositoryName,
      total: join$.call(totalTime, ' '),
      files: files.map(function(it){
        return fileStatus(it.trim().split(/\s+/), gitStatus);
      })
    };
  }
};
//# sourceMappingURL=/home/bartek/Projekty/atom/livescript-ide/timetracker/lib/gtm.js.map
