require! {
    \atom : { CompositeDisposable}
}

var exec

exec-in-repository = (repository, command, name) ->
    { exec } ?:= require 'child_process'
    options =
        cwd: repository.get-working-directory!

    exec command, options, (error, stdout) ->
        if error
          atom.notifications.add-error name,
              dismissable: true
              detail: error
        else
            atom.notifications.add-success name,
                dismissable: true
                detail: stdout

get-active-repository = ->
    if editor = atom.workspace.get-active-text-editor!
        active-path = editor.get-path!
        directories = atom.project.get-directories!filter (.contains active-path)
        if directory = directories.0
            atom.project.repository-for-directory directory

get-current-repository = ->>
    repository = await get-active-repository!
    unless repository
        repository = atom.project.get-repositories!0
    repository

exec-in-repo = (command, name) ->>
    if repository = await get-current-repository!
        exec-in-repository repository, command, name

var gtm
module.exports =
    activate: ->>
        require \atom-package-deps .install 'livescript-ide-timetracker'
        @disposables = new CompositeDisposable
            ..add atom.commands.add 'atom-workspace',
                'timetracker:status': -> exec-in-repo 'gtm status', 'timetracker:status'
                'timetracker:report': -> gtm.full-report!
                'timetracker:sync': -> exec-in-repo 'git fetchgtm && git pushgtm', 'timetracker:sync'
                'timetracker:merge': -> exec-in-repo '
                  git fetch origin refs/notes/gtm-data:refs/notes/origin/gtm-data \
                  && git notes --ref gtm-data merge -v origin/gtm-data \
                  && git pushgtm'
                'timetracker:stats' : @~toggle-stats
                'livescript-ide:push': -> exec-in-repo 'git pushgtm && git push', 'livescript-ide:push'
                'livescript-ide:fetch': -> exec-in-repo 'git fetch && git fetchgtm' 'livescript-ide:fetch'
        StatsView = require \./StatsView
        @stats-view = new StatsView!
        @stats-view-modal = await atom.workspace.add-modal-panel item: @stats-view.element, visible: false
        @stats-view.on \close_clicked !~>
            console.log \hiding
            @stats-view-modal.hide!
        stats = await atom.workspace.open new StatsView!
        pane = atom.workspace.pane-for-item stats
        gtm ?:= require \./gtm
        status = await gtm.status!
        stats.render {status}
        update = ->>
            status = await gtm.status!
            stats.render {status}
        @update-interval = set-interval update, 10000
        tab = pane.element.query-selector '[data-type="StatsView"] > .title'
            ..?class-list.add \icon \icon-clock

    deactivate: ->
        @disposables?dispose!
        clear-interval @update-interval
        @disposables = null

    toggle-stats: !->>
        if @stats-view-modal.is-visible!
            @stats-view-modal.hide!
        else
            status = await gtm.status!
            @stats-view.render {status}
            @stats-view-modal.show!
