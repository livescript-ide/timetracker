require! {
    \events : EventEmitter
}

class StatsView extends EventEmitter
    module.exports = @

    !->
        @element = document.create-element \div

    destroy: !-> @element.remove!

    get-URI: -> 'timetracker://stats'
    get-default-location: -> \right
    get-title: -> 'Timetracker (stats)'
    # dock requires this
    off: ->

    close-button: -> "
        <div class='block timetracker-stats-close-button'>
            <button class='btn'>Close</button>
        </div>
    "

    uncommited: (files = []) ->
        files.map -> "
            <p>
                <span class='status-#{it.type} icon icon-diff-#{it.type}'></span>
                <span class='timetracker-time'>#{it.time}</span>
                <span>#{it.percent} </span>
                <span>#{it.filename}</span>
            </p>"
        .join ''


    status: (status) ->
        unless status => ""
        else "
            <h2>#{status.name}</h2>
            <p>total: <span class='timetracker-time'>#{status.total}</span></p>
            <h2>Uncommited changes</h2>
            <div class='timetracker-files'>#{@uncommited status.files}</div>
        "

    render: (state) !->
        @element.inner-HTML = "
            <div class='timetracker-stats'>
                <h1><span class='icon icon-clock'></span></h1>
                #{@status state?status}
                #{@close-button!}
            </div>
        "
        @element.query-selector '.timetracker-stats-close-button' .add-event-listener \click  !~>
            @emit \close_clicked
