var exec

exec-in-repository = (repository, command, name) ->
    { exec } ?:= require 'child_process'
    options =
        cwd: repository.get-working-directory!
    (resolve,reject) <-! new Promise!
    exec command, options, (error, stdout) ->
        if error
        then reject error
        else resolve stdout

get-active-repository = ->
    if editor = atom.workspace.get-active-text-editor!
        active-path = editor.get-path!
        directories = atom.project.get-directories!filter (.contains active-path)
        if directory = directories.0
            atom.project.repository-for-directory directory

get-current-repository = ->>
    repository = await get-active-repository!
    unless repository
        repository = atom.project.get-repositories!0
    repository

exec-in-repo = (command, name) ->>
    if repository = await get-current-repository!
        exec-in-repository repository, command, name

parse-type: ->
    switch it
    | 'r' => \removed
    | 'modified' => \removed

map-git-status = ->
    switch it
    | \untracked => \added
    | _ => it ? \added

file-status = ([...time, percent, type,filename], git-status) ->
    filename: filename
    percent: percent
    time: time * ' '
    type: git-status.filter (.filename == filename) .map (.type) .0 |> map-git-status

parse-git-change-type = ->
    switch it
    | 'M'   => \modified
    | 'A'   => \added
    | 'AM'   => \added
    | 'R'   => \removed
    | '??'  => \untracked

git =
    status: ->>
        status = await exec-in-repo 'git status -s'
        status-lines = status.split '\n'
        status-lines.map (.trim!) .map (.split /\s+/) .map ([type,filename]) ->
            {filename, type: parse-git-change-type type}

parse-change = ([...time, percent, type,filename]) ->
    filename: filename
    percent: percent
    time: time * ' '
    type: type

parse-changes = (changes) ->
    changes.map ->
        it.trim!split /\s+/ |> parse-change

parse-commits = (lines) ->
    inside-commit = false
    empty-lines = 0
    commits = []
    current = []
    for line in lines
        if line == ""
            empty-lines++
            if empty-lines == 2
                commits.push current
                current = []
                empty-lines = 0
        else
            current.push line
    commits.map ([short-hash-and-message,date-and-author,...changes,summary]) ->
        result = {changes: parse-changes changes}
            ..short-hash = short-hash-and-message.substring 0, 7
            ..message = short-hash-and-message.substring 8
        result


module.exports =
    full-report: ->>
        report = await exec-in-repo 'git log --pretty=%H --since="100000 days ago" | gtm report'
        [ , ...report-commits, ] = report.split /\n/
        commits = parse-commits report-commits

    status: ->>
        [status, git-status] =  await Promise.all [
            exec-in-repo 'gtm status'
            git.status! ]
        status-lines = status .split '\n' .filter ->
            it != '[0;0m'
            and it != '[0m'
            and it != ""
        [...files, last-line] = status-lines
        [...total-time, repository-name] = last-line.trim! .split /\s+/
        name: repository-name
        total: total-time * ' '
        files: files.map ->
            it.trim!split /\s+/ |> file-status _, git-status
